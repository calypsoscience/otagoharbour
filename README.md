# Otago Harbour

High resolution hydrodynamic model of the Otago Harbour

## SCHISM MODEL

The domain extends from 6 km North of the harbor entrance to Dunedin and Andersons Bay. The average mesh resolution in the harbour is 30 m and 150 m near the open ocean boundary. To better resolve the hydrodynamics of the main channel, the model uses a mix of triangle and quadrilateral mesh

![SCHISM mesh](plots/Otago_mesh.png?raw=true "SCHISM mesh")

The domain was set up to run in 3D, with higher resolution near the surface to replicate the flows regime of interest to shipping. The vertical grid configuration is a Sigma type coordinate system with 10 layers.

## Boundaries

Elevation and current amplitudes and phases of the dominant tidal constituents (M2, S2, N2, K2, K1, O1, P1, Q1, MM, MF, M4, MN4, MS4, 2N2) were sourced from OTIS assimilated barotropic solutions (Oregon State University Tidal Inversion Software) model (produced by Oceanum Limited).

A second open boudary was set up for the Water of Leith river. However this boundary is not used in this barotropic simulation.

## Validation

The model was only validated using three tidal gauges: T-Shed, The spit and Port Chalmers.

The agreement between the model and the measured data is good - both in terms of the phases and magnitudes.

![validation](plots/validation.png?raw=true "SCHISM validation")

## Results

![results](plots/Otago2.gif?raw=true "SCHISM results")

## License
Shield: [![CC BY-NC-SA 4.0][cc-by-nc-sa-shield]][cc-by-nc-sa]

This work is licensed under a
[Creative Commons Attribution-NonCommercial-ShareAlike 4.0 International License][cc-by-nc-sa].

[![CC BY-NC-SA 4.0][cc-by-nc-sa-image]][cc-by-nc-sa]

[cc-by-nc-sa]: http://creativecommons.org/licenses/by-nc-sa/4.0/
[cc-by-nc-sa-image]: https://licensebuttons.net/l/by-nc-sa/4.0/88x31.png
[cc-by-nc-sa-shield]: https://img.shields.io/badge/License-CC%20BY--NC--SA%204.0-lightgrey.svg


