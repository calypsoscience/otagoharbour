# Otago Harbour

High resolution hydrodynamic model of the Otago Harbour

## Files

2dm2gr3.pl: to transform .2dm into a gr3 file 
otago_gridV0.2dm: the SMS grid file
otago_gridV0.sms: the SMS raw file

bathy/bathy_img.tif: Bathyemtry file done by merging SDB technique, LiDAR and char sounding data. Datum in Chart datum.

hgrid_withbathy.gr3: hgrid.gr3 with interpolated bathymetry (MSL)


