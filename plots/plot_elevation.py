import numpy as np
import os
import glob
import pandas as pd
import toto
import matplotlib.pyplot as plt
from datetime import timedelta,datetime

import matplotlib.gridspec as gridspec


measure_data='../tidal_gauges'
output_folder='../outputs'
start_time='2020-01-01'
output_timestep=60 # in minutes

### read station.in
schsim_folder='../inputs/'
station_file=glob.glob(os.path.join(schsim_folder,'station.in'))
station = pd.read_csv(station_file[0],parse_dates=False,header=None,skiprows=2,delimiter=' ',index_col=5)
station.rename(columns={1:'Easting',2:'Northing',3:'Depth'},index={5: 'Name'},inplace=True)
station.drop([0,4],axis=1,inplace=True)

### Read staion file from the run
station_output_file=glob.glob(os.path.join(output_folder,'staout_1*'))

station_output = pd.read_csv(station_output_file[0],
	parse_dates=False,header=None,index_col=0,delimiter='  ',skipfooter=2)

station_output['time']=pd.date_range(start = pd.to_datetime(start_time),
                                        periods = len(station_output), freq='%iT'%output_timestep)+timedelta(minutes=output_timestep)
station_output.set_index('time',inplace=True,drop=True)




plt.close('all')
fig = plt.figure()

gs1 = gridspec.GridSpec(3, 1)


I=0
for i,name in enumerate(station.index):


		filename=glob.glob(os.path.join(measure_data,name+'*.dat'))[0]
		print(filename)
		if os.path.isfile(filename):
			ax1 = fig.add_subplot(gs1[I])
			tx=pd.read_csv(filename,skiprows=4,header=None)
			tx['time'] = pd.to_datetime(tx[0], format='%Y-%m-%d %H:%M:%S')
			tx=tx.set_index('time')
			tx.index=tx.index-timedelta(hours=13)
			if 'TShed' in name:
				tx=tx[17]
			elif 'Spit' in name:
				tx=tx[16]
			elif 'PortChalmer' in name:
				tx=tx[14]
			tx=tx.rename('elev')
			df=tx.to_frame()
			df=df.resample('30T').mean()
			test=df.TideAnalysis.detide(mag='elev',
				args={'minimum SNR':2,'latitude':-45.8,'folder out':measure_data })
			df['elevt']=test['elevt']
			if i==2:		
				plt.plot(df.index-timedelta(hours=0),(df['elevt']-np.nanmean(df['elevt']))*1,'b',label='measured')
				plt.plot(station_output.index,station_output[i+1],label='modelled')
			else:
				plt.plot(df.index-timedelta(hours=0),(df['elevt']-np.nanmean(df['elevt']))*1,'b',)
				plt.plot(station_output.index,station_output[i+1])
					
			
			plt.xlim(np.min(station_output.index)+timedelta(days=1),np.max(station_output.index))
			plt.title(name)
			I+=1


fig.legend()
gs1.tight_layout(fig)
plt.savefig('validation.png')
plt.show()
